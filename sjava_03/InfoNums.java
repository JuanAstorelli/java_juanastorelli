public class InfoNums {

    public static void main(String[] args) {
        
        int[] nums = new int[args.length];
        for (int i = 0 ; i < args.length ; i++) {
            nums[i] = Integer.parseInt(args[i]);
        }

        System.out.println("El mayor es: " + mayor(nums));
        System.out.println("El menor es: " + menor(nums));
        System.out.println("La media es: " + media(nums));
    }

    private static int mayor(int[] nums) {

        int mayor = 0;
        for (int i = 0 ; i < nums.length ; i++) {

            if (i == 0) {
                mayor = nums[i];
            }
            else if (nums[i] > mayor) {
                mayor = nums[i];
            }
        }

        return mayor;
    }

    private static int menor(int[] nums) {
        
        int menor = 0;
        for (int i = 0 ; i < nums.length ; i++) {

            if (i == 0) {
                menor = nums[i];
            }
            else if (nums[i] < menor) {
                menor = nums[i];
            }
        }
        
        return menor;
    }

    private static float media(int[] nums) {
        
        float total = 0;
        for (int num : nums) {
            total += num;
        }

        return total / nums.length;
    }
}