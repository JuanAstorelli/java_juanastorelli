import java.util.Scanner;
import java.util.Random;

public class Adivina {

    public static void main(String[] args) {
        
        Scanner scan = new Scanner(System.in);
        Random random = new Random();
        int incognita = random.nextInt(1000) + 1;
        int resp = -1;
        int cont = 0;

        System.out.println("--BIENVENIDO A LA PARTIDA DE ADIVINA EL NÚMERO--");

        do {
            try {
                System.out.println("Dame un número del 1 al 1000: ");
                resp = scan.nextInt();

                if (resp > incognita) {
                    System.out.println("Te pasaste!");
                }
                else if (resp < incognita) {
                    System.out.println("No llegas!");
                }

                cont++;
            } catch (Exception e) {
                System.out.println("ERROR! Formato incorrecto en la entrada de teclado.");
                scan.next();
                resp = -1;
            }
        } while (incognita != resp);

        System.out.println("¡¡FELICIDADES!! Adivinaste el número. " + incognita);
        System.out.println("Número de intentos: " + cont);

        scan.close();
    }
}