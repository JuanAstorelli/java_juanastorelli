import java.util.Scanner;

public class Test {

    public static void main(String[] args) {
        
        Scanner scan = new Scanner(System.in);
        double vel;

        System.out.println("Introduce la velocidad: ");
        vel = scan.nextDouble();

        System.out.println("¿Qué conversión deseas realizar?");
        System.out.println("-MENÚ
                            \n\t1) Millas/hora a kilómetros/hora
                            \n\t2) ");
        int op = scan.nextInt();
        switch (op) {

            case 1:
                
                System.out.println(vel + "millas por hora son " + ConversorVel.mih2kmh(vel) + " metros por hora.");
                break;

            default:
                System.out.println("Opción inexistente.");
                break;
        }
    }
}