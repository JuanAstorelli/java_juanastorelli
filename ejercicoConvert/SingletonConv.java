public class SingletonConv {

    private static Conversor conv;

    public static Conversor getConv() {

        if (conv == null) {
            conv = new Conversor();
        }
            
        return conv;
    }
}