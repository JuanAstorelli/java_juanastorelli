import java.util.Scanner;

class Main {
    public static void main(String[] args) {
    
        Scanner scan = new Scanner(System.in);
        int km;

        /*Conversor con1 = new Conversor();
        Conversor con2 = new Conversor();*/

        Conversor con1 = SingletonConv.getConv();
        Conversor con2 = SingletonConv.getConv();

        System.out.println(con1);
        System.out.println(con2);
    }
}